#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

void pause();

void pause()
{
    int continuer = 1;
    SDL_Event event;
 
    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
        }
    }
}

int main
(
    int argc,
    char ** argv
)
{
    SDL_Surface * ecran = NULL;
    SDL_Surface * zebra = NULL;
    SDL_Surface * sapin = NULL;
    SDL_Surface * imageDeFond = NULL;

    SDL_Rect positionFond;
    SDL_Rect positionZebra;
    SDL_Rect positionSapin;

    positionFond.x = 0;
    positionFond.y = 0;

    positionZebra.x = 500;
    positionZebra.y = 260;
    SDL_BlitSurface(zebra, NULL, ecran, &positionZebra);

    positionSapin.x = 580;
    positionSapin.y = 270;

    if (SDL_Init(SDL_INIT_VIDEO) == -1) // Démarrage de la SDL. Si erreur :
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); // Écriture de l'erreur
        exit(EXIT_FAILURE); // On quitte le programme
    }

    /* Chargement de l'icône AVANT SDL_SetVideoMode */
    SDL_WM_SetIcon(IMG_Load("img/sdl_icone.bmp"), NULL);
 
    ecran = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32, SDL_HWSURFACE);
    if (ecran == NULL) // Si l'ouverture a échoué, on le note et on arrête
    {
        fprintf(stderr, "Impossible de charger le mode vidéo : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_WM_SetCaption("Display an image in a SDL window !", NULL);

    /* Chargement d'une image Bitmap dans une surface */
    imageDeFond = IMG_Load("img/lac_en_montagne.bmp");

    /* On blitte par-dessus l'écran */
    SDL_BlitSurface(imageDeFond, NULL, ecran, &positionFond);

    zebra = IMG_Load("img/zozor.bmp");
    SDL_SetColorKey(zebra, SDL_SRCCOLORKEY, SDL_MapRGB(zebra->format, 0, 0, 255));
    SDL_BlitSurface(zebra, NULL, ecran, &positionZebra);

    sapin = IMG_Load("img/sapin.png");
    SDL_BlitSurface(sapin, NULL, ecran, &positionSapin);

    SDL_Flip(ecran); /* Mise à jour de l'écran */

    pause();

    SDL_FreeSurface(imageDeFond); /* On libère la surface */
    SDL_FreeSurface(zebra);
    SDL_Quit();

    return EXIT_SUCCESS;

    exit(EXIT_SUCCESS);
}