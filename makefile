CXX?=g++
CPP?=gcc
CC?=gcc

CFLAGS=-O3
LDFLAGS=-lSDL -lSDL_mixer

BIN_PATH=bin
OBJ_PATH=obj
SRC_PATH=src

BIN_NAME=createSDLWindow

all : exe

clean:	
	rm -fr ${OBJ_PATH}/*.o

mrproper: clean
	rm -fr ${OBJ_PATH}/${BIN_NAME}

exe: $(BIN_PATH)/$(BIN_NAME)

$(BIN_PATH)/$(BIN_NAME): ${OBJ_PATH}/$(BIN_NAME).o
	mkdir -p $(BIN_PATH)
	$(CC) -o $(BIN_PATH)/$(BIN_NAME) ${OBJ_PATH}/$(BIN_NAME).o $(LDFLAGS)

${OBJ_PATH}/$(BIN_NAME).o: ${SRC_PATH}/$(BIN_NAME).c makefile
	mkdir -p $(OBJ_PATH)
	$(CC) -c ${SRC_PATH}/$(BIN_NAME).c -o ${OBJ_PATH}/$(BIN_NAME).o $(CFLAGS)
