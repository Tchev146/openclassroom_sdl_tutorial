#include <stdlib.h>
#include <stdio.h>
#include <SDL/SDL.h>

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

void pause();

void pause()
{
    int continuer = 1;
    SDL_Event event;
 
    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
            case SDL_QUIT:
                continuer = 0;
        }
    }
}
 
int main
(
    int argc, 
    char **argv
)
{
    SDL_Surface *ecran = NULL;
    SDL_Surface *rectangle = NULL;
    SDL_Rect position;

    int rectWidth = 220;
    int rectHeight = 180;

    if (SDL_Init(SDL_INIT_VIDEO) == -1) // Démarrage de la SDL. Si erreur :
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); // Écriture de l'erreur
        exit(EXIT_FAILURE); // On quitte le programme
    }
 
    ecran = SDL_SetVideoMode(WINDOW_WIDTH, WINDOW_HEIGHT, 32, SDL_HWSURFACE);
    if (ecran == NULL) // Si l'ouverture a échoué, on le note et on arrête
    {
        fprintf(stderr, "Impossible de charger le mode vidéo : %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }

    SDL_WM_SetCaption("My first SDL Window !", NULL);
    SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 17, 206, 112));

    rectangle = SDL_CreateRGBSurface(SDL_HWSURFACE, rectWidth, rectHeight, 32, 0, 0, 0, 0);
    SDL_FillRect(rectangle, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

    position.x = (WINDOW_WIDTH/2) - (rectWidth/2); 
    position.y = (WINDOW_HEIGHT/2) - (rectHeight/2);

    SDL_BlitSurface(rectangle, NULL, ecran, &position); // Collage de la surface sur l'écran

    SDL_Flip(ecran); /* Mise à jour de l'écran */

    pause();

    SDL_FreeSurface(rectangle);
    SDL_Quit();
 
    exit(EXIT_SUCCESS);
}